%undefine _debugsource_packages

Summary: A new type of shell
Name: nushell
Version: 0.84.0
Release: 2
License: MIT
Group: Development/Shell
URL: https://www.nushell.sh/
Source0: https://github.com/nushell/nushell/archive/refs/tags/%{version}.tar.gz#/%{name}-%{version}.tar.gz
BuildRequires: openssl-devel
BuildRequires: cargo

%description
Nushell (or Nu for short) is a new shell that takes a modern, structured
approach to the commandline.

%prep
%setup -q

%build
cargo build --release

%install
cargo install --root=%{buildroot}%{_prefix} --path=.

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%doc LICENSE *.md
%{_bindir}/nu
%exclude /usr/.crates*

%post
if [ "$1" = 1 ]; then
  if [ ! -f /etc/shells ] ; then
    touch /etc/shells
  fi
  for binpath in /usr/bin /bin; do
    if ! grep -q "^${binpath}/nu$" /etc/shells; then
       (cat /etc/shells; echo "$binpath/nu") > /etc/shells.new
       mv /etc/shells{.new,}
    fi
  done
fi

%postun
if [ "$1" = 0 ] && [ -f /etc/shells ] ; then
  sed -e '\!^/usr/bin/nu$!d' -e '\!^/bin/nu$!d' < /etc/shells > /etc/shells.new
  mv /etc/shells{.new,}
fi

%changelog
* Fri Aug 25 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.84.0-2
- Added scriptlets for adding nushell to /etc/shells

* Fri Aug 25 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.84.0-1
- Update to version 0.84.0

* Sun Jun 11 2023 Wei-Lun Chao <bluebat@member.fsf.org> - 0.81.0
- Rebuilt for Fedora
